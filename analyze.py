import numpy as np
import sys

CLOCK_CYCLE_LENGTH = 63
FRAME_GAP = 7  # minimum number of cycles between frames
FUNCTION_TAGS =[
    [1, 0, 0, 1, 0, 1, 0, 0],  # OPEN_TAG
    [0, 1, 1, 1, 0, 0, 1, 1]   # CLOSE_TAG
]
FUNCTION_NAMES = [
    "OPEN", "CLOSE"
]

def print_info(msg):
    print(f'[ \033[92mINFO\033[00m ] {msg}')


def print_warn(msg):
    print(f'[ \033[91mWARN\033[00m ] {msg}')


# convert to binary and resample at rate
def resample(array, rate):
    threshold = array.mean()
    return (array[::rate]) > threshold


# splits the array on all runs of 0 are that are longer than gap_thresh
# assumes the array starts with zero
def get_frames(np_array, gap_thresh):
    frames = []
    last_one = -1
    in_frame = False
    for i, item in enumerate(np_array):
        if item == 0:
            if i - last_one > gap_thresh and in_frame:
                in_frame = False
                frames.append(last_one + 1)
        if item == 1:
            if not in_frame:
                in_frame = True
                frames.append(i)
            last_one = i
    return np.split(np_array, frames)[1::2]


# produces a np array of given length of alternating ones and zeros
def clock_signal(length):
    zeros = np.zeros(length, dtype=int)
    zeros[::2] = 1
    return zeros


def manchester_decode(frame):
    return np.bitwise_xor(frame, clock_signal(len(frame)))[1::2]


def get_function(function_tag):
    tag_correlation = [ np.correlate(function_tag, FUNCTION_TAGS[i])
                        for i in range(len(FUNCTION_TAGS))]
    return f'{FUNCTION_NAMES[tag_correlation.index(max(tag_correlation))]}'



def main():
    if len(sys.argv) != 2:
        print("Usage: analyze.py <filename>")
        exit()
    # start up message
    print_info(f'analyzing {sys.argv[1]}')
    raw_data = np.fromfile(sys.argv[1])
    frames = [ manchester_decode(frame) for frame in
               get_frames(resample(raw_data, CLOCK_CYCLE_LENGTH), FRAME_GAP)]
    if len(frames) % 8 != 0:
        print_warn(f'wrong number of frames: {len(frames)}')
        exit()
    messages = np.split(np.array(frames), np.arange(0,len(frames),8)[1:])
    for i,message in enumerate(messages):
        for clock_sync_msg in message[::2]:
            # clock sync msg is 10 bits long
            if not np.array_equal(clock_sync_msg, clock_signal(11)[1:]):
                print_warn('clock sync msg not synced')
        # first 8 bytes of message are sync
        for frame in message[1::2]:
            if not np.array_equal(frame[:8], clock_signal(9)[1:]):
                print_warn(f'message frame not synced')
        # print the message type
        function_tag = get_function(message[1][8:16])
        # print the message data
        binary_array = [frame[16:] for frame in message[1::2]]
        binary_array = np.array([item for sublist in binary_array for item in sublist])
        int_array = [ int(''.join(str(bit) for bit in bits), 2) for bits in
                      np.split(binary_array, np.arange(0, len(binary_array), 8))[1:]]
        data = ''.join('{:02x}'.format(x) for x in int_array)
        print_info(f'{function_tag} message: \n\t{data}')





if __name__ == '__main__':
    main()
